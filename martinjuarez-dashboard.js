{
  const {
    html,
  } = Polymer;
  /**
    `<martinjuarez-dashboard>` Description.

    Example:

    ```html
    <martinjuarez-dashboard></martinjuarez-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --martinjuarez-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class MartinjuarezDashboard extends Polymer.Element {

    static get is() {
      return 'martinjuarez-dashboard';
    }

    static get properties() {
      return {

        mialumno: {
          type: Object,
          notify: true,
          value: { }
        },
        inputName: {
          type: String,
          notify: true
        },
        poke: {
          type: Object,
          value: { }
        }

      };
    }
    alumnoName(string) {
      if (!string) {
        return null;
      } else {
        string = string.toLowerCase();
        return function(item) {
          var first = item.name.toLowerCase();
          var last = item.last.toLowerCase();
          return (first.indexOf(string) !== -1 ||
              last.indexOf(string) !== -1);
        };
      }
    }

    ajaxResponse(e) {
      this.poke = e.detail.response;
    }
    static get template() {
      return html `
      <style include="martinjuarez-dashboard-styles martinjuarez-dashboard-shared-styles"></style>
      <slot></slot>
        <iron-ajax
        auto
        url="https://pokeapi.co/api/v2/pokemon/pikachu/"
        handle-as="json"
        on-response="ajaxResponse">
        </iron-ajax>



          <p>Welcome to Hell</p>
          <p>{{poke.name}}</p>


          <div class="search">
            <label>Nombre del Alumno :</label><br>
              <input type="text" name="filter-input" id="filter-input" value="{{inputName::input}}">
          </div>
          <br><br>
          <cells-mocks-component alumnos="{{mialumno}}"><cells-mocks-components>
          <div>

          <template is="dom-repeat" items="{{mialumno}}" filter="{{alumnoName(inputName)}}">
            <div class="card">
             <span><img src="{{item.img}}" width="100px"  height="100px"></span><br>
              Nombre : <span>{{item.name}}</span><br>
              Apellidos :<span>{{item.last}}</span><br>
              Dirección :<span>{{item.address}}</span><br>
              Hobbies :<span>{{item.hobbies}}</span>
              <br><br>
            </div>
          </template>
        </div>







      `;
    }

  }

  customElements.define(MartinjuarezDashboard.is, MartinjuarezDashboard);
}
